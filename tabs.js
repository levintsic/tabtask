function clickTab(index) {
  var tabs = document.querySelectorAll(".tab");
  var lis = document.querySelectorAll("nav>li");
  for (var ti = 0; ti < tabs.length; ti++) {
    tabs[ti].classList.remove("active");
    lis[ti].classList.remove("active");

    if (ti === index) {
      tabs[ti].classList.add("active");
      lis[ti].classList.add("active");
    }
  }
}